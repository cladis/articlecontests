/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.ceespring.objects;

import java.util.Date;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class CEESTableLine {

    public String wname;
    public int pageid;
    public String article;
    public String userid;
    public String caid;
    public String user;
    public String gender;
    public String isnoob;
    public int size;
    public int words;
    public int references;
    public String country;
    public String topic;
    public String humanity;
    public String female;
    public java.sql.Date timestamp;
    public String wdentity;
    public boolean isfromthelist;
    public String isimproved;
    public int inweek;
    public int bytessigma;
    public int weeksigma;
    public int weekoldid;
    public int authoroldid;
    public String badge;

    public CEESTableLine(
            String wname_p, int pageid_p, String article_p,
            /*String userid_p, String caid_p,*/ String user_p, /*String gender_p,
            String isnoob_p,*/ int size_p, int words_p, int references_p,
            String country_p, String topic_p,
            String humanity_p, String female_p, Date timestamp_p,
            String wdentity_p, boolean isfromthelist_p, String isimproved_p,
            int inweek_p, int bytessigma_p, int weeksigma_p, int authoroldid_p,
            int weekoldid_p, String badge_p
    ) {
        this.wname = wname_p;
        this.pageid = pageid_p;
        this.article = article_p;
//        this.userid = userid_p;
//        this.caid = caid_p;
        this.user = user_p;
//        this.gender = gender_p;
//        this.isnoob = isnoob_p;
        this.size = size_p;
        this.words = words_p;
        this.references = references_p;
        this.country = country_p;
        this.topic = topic_p;
        this.humanity = humanity_p;
        this.female = female_p;
        this.timestamp = new java.sql.Date(timestamp_p.getTime());
        this.wdentity = wdentity_p;
        this.isfromthelist = isfromthelist_p;
        this.isimproved = isimproved_p;
        this.inweek = inweek_p;
        this.bytessigma = bytessigma_p;
        this.weeksigma = weeksigma_p;
        this.weekoldid = weekoldid_p;
        this.authoroldid = authoroldid_p;
        this.badge = badge_p;
    }
;
}
