/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.ceespring.out;

import basebot.ceespring.DatabasePart;
import java.sql.ResultSet;
import org.wikipedia.BaseBot;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class outPartCountries {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        BaseBot m = new BaseBot("meta.wikimedia.org");
        m.setUserAgent("BaseBot");
        m.login(args[0], args[1]);
        m.setMarkBot(true);
        m.setMarkMinor(true);

        BaseBot c = new BaseBot("commons.wikimedia.org");
        c.setUserAgent("BaseBot");
        c.login(args[0], args[1]);
        c.setMarkBot(true);
        c.setMarkMinor(true);

        DatabasePart db = new DatabasePart();

        String[][][] instances = {{
            {"country", "countries"},
            {"Albania", "Armenia", "Austria", "Azerbaijan",
                "Republic of Bashkortostan", "Belarus", "Bosnia and Herzegovina",
                "Bulgaria", "Croatia", "Czech Republic", "Esperantujo",
                "Estonia", "Georgia", "Greece", "Hungary", "Kazakhstan",
                "Kosovo", "Latvia", "Lithuania", "Macedonia", "Moldova", "Poland",
                "Romania", "Russia", "Sakha Republic", "Republika Srpska",
                "Serbia", "Slovakia", "Turkey", "Ukraine"}
        },
        {
            {"topic", "topics"},
            {"Culture",
                "Earth", "Economics", "Society", "Sports", "Politics",
                "Transport", "History", "Science", "Education", "Women"}
        }};

        for (String[][] instance1 : instances) {
            System.out.println("Instances length: " + instances.length);
            String[][] instance = instance1;
            String singular = instance[0][0];
            String plural = instance[0][1];
            String[] items = instance[1];
            String query = ""
                    + "SELECT *\n"
                    + "FROM (\n";
            for (int i = 0; i < items.length; i++) {
                String item = items[i];
                query += i != 0
                        ? "\n   UNION ALL\n" : "";
                query += ""
                        + "   SELECT \"" + item + "\" AS " + singular + ",\n"
                        + "   SUM(CASE WHEN `article_" + singular + "` LIKE \"%" + item + "%\" THEN 1 ELSE 0 END ) as articles,\n"
                        + "   SUM(CASE WHEN `article_" + singular + "` LIKE \"%" + item + "%\" AND `article_isimproved` = \"no\" THEN 1 ELSE 0 END) as created,\n"
                        + "   SUM(CASE WHEN `article_" + singular + "` LIKE \"%" + item + "%\" AND `article_isimproved` = \"yes\" THEN 1 ELSE 0 END) as improved\n"
                        + "   FROM `2016`";
            }
            query += ""
                    + ") AS tablie\n"
                    + "ORDER BY articles DESC";
            System.out.println(query);
            ResultSet generalOutputtie = db.generalOutputtie(query);
            String wrt = "The page lists sums of articles created and improved in the whole contest about specific " + plural + ".\n\n"
                    + "The data comes from the <code>" + singular + "</code> parameter of the contest talk page template through "
                    + "categories the template puts the talk pages into. If you use the model described but saw in "
                    + "the [[User:BaseBot/CEES/MMXVI/General output|General output]] that the data is missing for your wiki, then probably "
                    + "the categories mentioned are not linked to Wikidata: you can fix that by linking them to the entities listed on "
                    + "[[User:BaseBot/CEES/MMXVI/Categories]] page.";
            wrt += "\n\n\n";
            String graph_items = "";
            String graph_articles = "";
            String graph_created = "";
            String graph_improved = "";
            String table = "{{User:BaseBot/CEES/MMXVI/Per " + singular + " sums (general)/header}}\n";

            //Tabular data test
            String tabular_graph = "{\"license\": \"CC0-1.0+\",\"description\": {\"en\": \"table description\"},\"sources\": \"Tool labs replication and Wikimedia API\",\"schema\": {\"fields\": ["
                    + "{\"name\": \"" + singular + "\",\"type\": \"string\",\"title\": {\n"
                    + "\"en\": \"" + singular + "\"}},"
                    + "{\"name\": \"articles\",\"type\": \"number\",\"title\": {\"en\": \"articles\",\"uk\": \"статей\"}},"
                    + "{\"name\": \"created\",\"type\": \"number\",\"title\": {\"en\": \"created\",\"uk\": \"створено\"}},"
                    + "{\"name\": \"improved\",\"type\": \"number\",\"title\": {\"en\": \"improved\",\"uk\": \"покращено\"}}]},\n"
                    + "\"data\": [\n";
            while (generalOutputtie.next()) {

                table += "|-\n";
                table += "| " + generalOutputtie.getString(1) + "\n";
                table += "| " + generalOutputtie.getInt(2) + "\n";
                table += "| " + generalOutputtie.getInt(3) + "\n";
                table += "| " + generalOutputtie.getInt(4) + "\n";

                graph_items += graph_items.equals("") ? "" : ", ";
                graph_items += generalOutputtie.getString(1);

                graph_articles += graph_articles.equals("") ? "" : ", ";
                graph_articles += generalOutputtie.getInt(2);

                graph_created += graph_created.equals("") ? "" : ", ";
                graph_created += generalOutputtie.getInt(3);

                graph_improved += graph_improved.equals("") ? "" : ", ";
                graph_improved += generalOutputtie.getInt(4);

                tabular_graph += "[\"" + generalOutputtie.getString(1)
                        + "\"," + generalOutputtie.getInt(2)
                        + "," + generalOutputtie.getInt(3)
                        + "," + generalOutputtie.getInt(4) + "],\n";

            }

            tabular_graph += "    ]}";
            table += "|}\n\n";
            String graph = "{{User:BaseBot/CEES/MMXVI/Per " + singular + " sums (general)/graph\n";
            graph += "|" + plural + " = " + graph_items + "\n";
            graph += "|created   = " + graph_created + "\n";
            graph += "|improved  = " + graph_improved + "\n";
            graph += "}}";
            String pie = "{{User:BaseBot/CEES/MMXVI/Per " + singular + " sums (general)/pie\n";
            pie += "|" + plural + " = " + graph_items + "\n";
            pie += "|articles   = " + graph_articles + "\n";
            pie += "}}";
            wrt += "\n== Table ==\n";
            wrt += "\n" + table + "\n\n";
            wrt += "== Graph ==\n";
            wrt += "\n=== Bars ===\n";
            wrt += "\n\n" + graph + "\n\n";
            wrt += "\n=== Pie  ===\n";
            wrt += "\n\n" + pie + "\n\n";
            wrt += "";
            // m.edit("User:BaseBot/CEES/MMXVI/Per " + singular + " sums (general)", wrt, "creating/updating");
            c.edit("User:BaseBot/Data:CEE Srping statistics/MMXVI/Per " + singular + " sums (general).tab", tabular_graph, "test creating/updating");
            try {
                c.edit("Data:CEE Srping statistics/MMXVI/Per " + singular + " sums (general).tab", tabular_graph, "test creating/updating");
            } catch (Exception ex) {
                System.out.println("Meh:" + ex.getMessage());
            }
            System.out.println(wrt);

        }

        db.stopDB();
    }

}
