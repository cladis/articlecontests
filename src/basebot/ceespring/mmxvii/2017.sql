-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Авг 23 2017 г., 06:55
-- Версия сервера: 5.5.27
-- Версия PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `cees`
--

-- --------------------------------------------------------

--
-- Структура таблицы `2017`
--

CREATE TABLE IF NOT EXISTS `2017` (
  `counter` int(11) NOT NULL AUTO_INCREMENT,
  `wiki` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `article_id` int(11) NOT NULL,
  `article_title` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `author_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `author_ca` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `author_gender` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `author_isnoob` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `article_bytes` int(11) NOT NULL,
  `article_words` int(11) NOT NULL,
  `article_country` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `article_topic` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `article_isperson` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `article_iswoman` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `article_creationdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `article_wdentity` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `article_fromlist` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `article_isimproved` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `article_fromweek` int(11) NOT NULL,
  `article_authorbytessigma` int(11) NOT NULL,
  `article_authorbytesweeksigma` int(11) NOT NULL,
  `article_authoroldid` int(11) NOT NULL,
  `article_authorweekoldid` int(11) NOT NULL,
  `article_badge` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `article_references` int(11) NOT NULL,
  UNIQUE KEY `counter` (`counter`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7618 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
