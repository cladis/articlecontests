/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.ceespring.mmxvii;

import basebot.ceespring.*;
import java.math.BigDecimal;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class CEESUAMarkedArticleVII {

    //Gathered data
    public String articleID;
    public String articleName;

    public String author;

    public int added;

    public String topic;
    public int week;

    public Boolean fromLists;
    public Boolean isList;
    public Boolean isDisqualified;

    public BigDecimal Mcoffsky;
    public BigDecimal Стефанко1982;
    public BigDecimal Zheliba;
    public BigDecimal Звірі;
    public BigDecimal Dim_Grits;

    //Computed data
    public BigDecimal qualityCoefficient;
    public BigDecimal quantityCoefficient;
    public BigDecimal additionalCoefficient;

    public BigDecimal totalPoints;
    public BigDecimal totalPointsWithCeiledQuality;
    public BigDecimal totalPointsWithQualityTwo;

    public CEESUAMarkedArticleVII(
            String articleID,
            String articleName,
            String author,
            int added,
            String topic,
            int week,
            Boolean fromLists,
            Boolean isList,
            Boolean isDisqualified,
            double Mcoffsky,
            double Стефанко1982,
            double Zheliba,
            double Звірі,
            double Dim_Grits
    ) {
        this.articleID = articleID;
        this.articleName = articleName;
        System.out.println("articleName = " + articleName);

        this.author = author;
        this.added = added;
        this.topic = topic;
        this.week = week;

        this.fromLists = fromLists;
        this.isList = isList;
        this.isDisqualified = isDisqualified;
        this.Mcoffsky = new BigDecimal(Mcoffsky);
        this.Стефанко1982 = new BigDecimal(Стефанко1982);
        this.Zheliba = new BigDecimal(Zheliba);
        this.Звірі = new BigDecimal(Звірі);
        this.Dim_Grits = new BigDecimal(Dim_Grits);

        this.qualityCoefficient = averageMark(this.Mcoffsky, this.Стефанко1982, this.Zheliba, this.Звірі, this.Dim_Grits);
        System.out.println("qualityCoefficient = " + qualityCoefficient);
        this.quantityCoefficient = quantity();
        System.out.println("quantityCoefficient = " + quantityCoefficient);

        this.additionalCoefficient = new BigDecimal(fromLists ? 2 : 1);
        System.out.println("additionalCoefficient = " + additionalCoefficient);

        this.totalPoints = computeTotal(0);
        System.out.println("totalPoints = " + totalPoints);

        this.totalPointsWithCeiledQuality = computeTotal(1);
        System.out.println("totalPointsWithCeiledQuality = " + totalPointsWithCeiledQuality);

        this.totalPointsWithQualityTwo = computeTotal(2);
        System.out.println("totalPointsWithQualityTwo = " + totalPointsWithQualityTwo);

    }

    private BigDecimal averageMark(BigDecimal... number) {
        BigDecimal many = new BigDecimal(0);
        BigDecimal sigma = new BigDecimal(0);
        for (BigDecimal d : number) {
            if (d.compareTo(new BigDecimal(-1)) == 1) {
                many = many.add(new BigDecimal(1));
                System.out.println("I'm gonna add " + d);
                sigma = sigma.add(d);
            }
        }
        System.out.println("many: " + many + "\tsigma: " + sigma);
        return (sigma.compareTo(new BigDecimal(0)) == 0) ? new BigDecimal(0) : sigma.divide(many);
    }

    private BigDecimal quantity() {

        BigDecimal quantity = new BigDecimal(0);
        if (added >= 3500 && (isList || added < 5000)) {

            quantity = new BigDecimal(0.8);
        } else if (added >= 5000 && added < 10000) {

            quantity = new BigDecimal(1);
        } else if (added >= 10000) {

            quantity = new BigDecimal(1.1);
        }

        return quantity;
    }

    private BigDecimal computeTotal(int ceilQuality) {
        System.out.println("Size divided by 1000 = " + ((new BigDecimal(added)
                .divide(new BigDecimal(1000)))));
        BigDecimal total = ((new BigDecimal(added)
                .divide(new BigDecimal(1000)))
                .multiply(
                        (ceilQuality == 1
                        ? qualityCoefficient.setScale(0, BigDecimal.ROUND_CEILING)
                        : (ceilQuality == 2
                        ? new BigDecimal(2)
                        : qualityCoefficient))
                )
                .multiply(quantityCoefficient)
                .multiply(additionalCoefficient));
        return total;
    }

}
