package basebot.ceespring.mmxvii;

import basebot.ceespring.objects.*;

/**
 * @author Base <base-w at yandex.ru>
 */
public class CEESParsedUkwikiListedArticleVII {

    //I use talks and not just articles just so that the code is the same for the case
    public String id;
    public String isalist;
    public String disqualified;
    public String notes;
    public String Mcoffsky;
    public String Стефанко1982;
    public String Zheliba;
    public String Звірі;
    public String Dim_Grits;
    public String highlight;

    public CEESParsedUkwikiListedArticleVII(
            String id_p,
            String isalist_p,
            String disqualified_p,
            String notes_p,
            String Mcoffsky_p,
            String Стефанко1982_p,
            String Zheliba_p,
            String Звірі_p,
            String Dim_Grits_p,
            String highlight_p) {
        this.id = id_p;
        this.isalist = isalist_p;
        this.disqualified = disqualified_p;
        this.notes = notes_p;
        this.Mcoffsky = Mcoffsky_p;
        this.Стефанко1982 = Стефанко1982_p;
        this.Zheliba = Zheliba_p;
        this.Звірі = Звірі_p;
        this.Dim_Grits = Dim_Grits_p;
        this.highlight = highlight_p;
    }
;
}
