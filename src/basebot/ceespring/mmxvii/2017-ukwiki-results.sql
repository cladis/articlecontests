-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Июл 18 2016 г., 01:44
-- Версия сервера: 5.5.27
-- Версия PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `cees`
--

-- --------------------------------------------------------

--
-- Структура таблицы `2016-ukwiki-results`
--

CREATE TABLE IF NOT EXISTS `2017-ukwiki-results` (
  `counter` int(9) NOT NULL AUTO_INCREMENT,
  `article_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `article_name` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `article_author` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `article_topic` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `article_week` int(1) NOT NULL,
  `article_mark` float NOT NULL,
  `article_mark_ceiled_quality` float NOT NULL,
  `article_mark_quality_two` float NOT NULL,
  PRIMARY KEY (`counter`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=0 ;

