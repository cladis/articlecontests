/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.ceespring;

import objects.Credentials;
import java.sql.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import java.util.HashMap;
import java.util.Map;
import basebot.ceespring.objects.CEESUser;

/**
 *
 * @author Base <base-w at yandex.ru>
 *
 * "u2785", "feeshahroheefuth"
 */
public class ReplicationFetcher {

    String strSshUser = ""; // SSH loging username
    String strSshHost = "tools-login.wmflabs.org"; // hostname or ip or SSH server
    String strRemoteHost = "cswiki.labsdb"; // hostname or ip of your database server
    int nLocalPort = 3366; // local port number use to bind SSH tunnel
    int nRemotePort = 3306; // remote port number of your database
    String strDbUser = ""; // database loging username
    String strDbPassword = ""; // database login password
    static String sshKeyPath = "";
    static String sshKeyPassphrase = "";

    /**
     *
     * @param c
     */
    public ReplicationFetcher(Credentials c) {
        strSshUser = c.shelllogin;
        strDbUser = c.labsdblogin;
        strDbPassword = c.labsdbpassword;
        sshKeyPath = c.sshkeypath;
        sshKeyPassphrase = c.sshpassphrase;
    }

    private static Session doSshTunnel(
            String strSshUser, String strSshHost,
            String strRemoteHost, int nLocalPort, int nRemotePort
    ) throws Exception {
        final JSch jsch = new JSch();
        jsch.addIdentity(sshKeyPath, sshKeyPassphrase);
        Session session = jsch.getSession(strSshUser, strSshHost, 22);
        final Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        session.connect();
        session.setPortForwardingL(nLocalPort, strRemoteHost, nRemotePort);
        System.out.println("Is connected: " + session.isConnected());
        return session;
    }

    public Map<String, String> fetchCoursesArticles(String wikidb, String[] courses) {
        Map<String, String> cs_articles = new HashMap<>();
        try {

            Session doSshTunnel = ReplicationFetcher.doSshTunnel(strSshUser, strSshHost, strRemoteHost, nLocalPort,
                    nRemotePort);

            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://" + "localhost:" + nLocalPort + "?useUnicode=true&characterEncoding=utf8",
                    strDbUser, strDbPassword);

            String db = wikidb + "_p";//"cswiki_p";
            String query
                    = "select " + db + ".ep_courses.course_id,"
                    + db + ".ep_courses.course_title,"
                    + db + ".ep_articles.article_page_title,"
                    + db + ".user.user_name\n"
                    + "from " + db + ".ep_articles\n"
                    + "join " + db + ".ep_courses on " + db + ".ep_courses.course_id  = " + db + ".ep_articles.article_course_id\n"
                    + "join " + db + ".user on " + db + ".user.user_id = " + db + ".ep_articles.article_user_id\n"
                    + "where " + db + ".ep_courses.course_title in\n"
                    + "(\n";
            for (int i = 0; i < courses.length; i++) {
                String course = courses[i];
                query += "  \"" + course + "\"" + (i == courses.length - 1 ? "" : ",") + "\n";
            }
            query += ")";
            System.out.println(query);

            Statement m_Statement = con.createStatement();
            ResultSet m_ResultSet = m_Statement.executeQuery(query);
            while (m_ResultSet.next()) {
                int course_id = m_ResultSet.getInt(1);
                String course_title = new String(m_ResultSet.getBytes(2), "UTF-8");
                String article_page_title = new String(m_ResultSet.getBytes(3), "UTF-8");;
                String user_name = new String(m_ResultSet.getBytes(4), "UTF-8");
                //cs_articles.add(new CEESParsedUkwikiListedArticle("Talk:" + article_page_title, user_name));
                cs_articles.put("Talk:" + article_page_title, user_name);
                System.out.println("Course\t#" + course_id + "\t" + course_title + ",\tarticle\t" + article_page_title + "\tby\t" + user_name);
            }
            con.close();
            doSshTunnel.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(665);
        }
        return cs_articles;

    }

    public Map<String, CEESUser> fetchUsersData(int port, String wikidb, String starttime, String noobtime, String... users) {
        Map<String, CEESUser> usersdata = new HashMap<>();
        if(users.length == 0){
            System.out.println(wikidb+"\tfetchUsersData: no users");
            return usersdata;
        }
        int localport = (port == -1 ? nLocalPort : port);
        try {
            Session doSshTunnel = ReplicationFetcher.doSshTunnel(
                    strSshUser,
                    strSshHost,
                    strRemoteHost,
                    localport,
                    nRemotePort
            );
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://" + "localhost:" + localport + "?useUnicode=true&characterEncoding=utf8",
                    strDbUser,
                    strDbPassword
            );

            String db = wikidb + "_p";//"cswiki_p";
            String query = ""
                    + "SELECT	r.rev_user_text AS username,\n"
                    + "		(select sum(case when rev_timestamp < " + noobtime + " then 1 else 0 end)\n"
                    + "         from " + db + ".revision_userindex\n"
                    + "         where rev_user_text = r.rev_user_text\n"
                    + "         group by rev_user_text\n"
                    + "         limit 1\n"
                    + "        ) AS edits_in_timeframe,\n"
                    + "        IFNULL(up_value, \"unknown\"),\n"
                    + "        user_id,\n"
                    + "        gu_id\n"
                    + "FROM " + db + ".revision_userindex r\n"
                    + "JOIN " + db + ".user u\n"
                    + "ON u.user_name = r.rev_user_text\n"
                    + "LEFT JOIN " + db + ".user_properties pr\n"
                    + "ON pr.up_user = u.user_id AND up_property = \"gender\" \n"
                    + "JOIN centralauth_p.globaluser gu\n"
                    + "ON gu.gu_name = r.rev_user_text\n"
                    + "WHERE rev_timestamp > " + starttime + " AND rev_user_text IN (\n";

            
            for (int i = 0; i < users.length; i++) {
                String user = users[i];
                query += "  \"" + user + "\"" + (i == users.length - 1 ? "" : ",") + "\n";
            }
            query += ""
                    + ")\n"
                    + "GROUP BY rev_user_text\n"
                    + "ORDER BY username ASC";
            System.out.println(query);

            Statement m_Statement = con.createStatement();
            ResultSet m_ResultSet = m_Statement.executeQuery(query);

            //username	edits_in_timeframe	up_value	user_id	gu_id
            //CEESUser(String id, String CAId, String username, String sex, String noob)
            while (m_ResultSet.next()) {
                String username = new String(m_ResultSet.getBytes(1), "UTF-8");
                int edits_in_timeframe = m_ResultSet.getInt(2);
                String isnoob = edits_in_timeframe > 0 ? "no" : "yes";
                String gender = new String(m_ResultSet.getBytes(3), "UTF-8");
                String user_id = new String(m_ResultSet.getBytes(4), "UTF-8");
                String gu_id = new String(m_ResultSet.getBytes(5), "UTF-8");
                //cs_articles.add(new CEESParsedUkwikiListedArticle("Talk:" + article_page_title, user_name));
                CEESUser ceesUser = new CEESUser(user_id, gu_id, username, gender, isnoob);
                usersdata.put(username, ceesUser);
                System.out.println("Fetcher:\t" + ceesUser);
                // System.out.println("Course\t#" + course_id + "\t" + course_title + ",\tarticle\t" + article_page_title + "\tby\t" + user_name);
            }

            con.close();
            doSshTunnel.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(667);
        }
        return usersdata;

    }

    public Map<String, CEESUser> fetchArticlesData(int port, String wikidb, String... talks) {
        Map<String, CEESUser> articlesdata = new HashMap<>();
        int localport = (port == -1 ? nLocalPort : port);
        try {
            Session doSshTunnel = ReplicationFetcher.doSshTunnel(
                    strSshUser,
                    strSshHost,
                    strRemoteHost,
                    localport,
                    nRemotePort
            );
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://" + "localhost:" + localport + "?useUnicode=true&characterEncoding=utf8",
                    strDbUser,
                    strDbPassword
            );

            String db = wikidb + "_p";//"cswiki_p";
            String query = ""
                    + "select rev_id, rev_page, page_title, ips_item_id, rev_user, rev_user_text, rev_timestamp, rev_len, rev_parent_id, rev_sha1\n"
                    + "from " + db + ".revision_userindex  r\n"
                    + "join " + db + ".page p\n"
                    + "on p.page_id = r.rev_page\n"
                    + "left join wikidatawiki_p.wb_items_per_site d\n"
                    + "on ips_site_id = \"" + wikidb + "\" and p.page_title = replace(d.ips_site_page, \" \", \"_\")\n"
                    + "where page_title IN (\n";
            for (int i = 0; i < talks.length; i++) {
                String talk = talks[i];
                String article = talk.substring(talk.indexOf(":") + 1);
                query += "  \"" + article.replaceAll(" ", "_").replaceAll("\"", article) + "\"" + (i == talks.length - 1 ? "" : ",") + "\n";
            }
            query += ""
                    + ")\n"
                    + "order by rev_timestamp asc\n";
            System.out.println(query);

            Statement m_Statement = con.createStatement();
            ResultSet m_ResultSet = m_Statement.executeQuery(query);

            //	rev_page    page_title  ips_item_id rev_user    rev_user_text   rev_timestamp   rev_len rev_parent_id   rev_sha1
//            //CEESUser(String id, String CAId, String username, String sex, String noob)
//            while (m_ResultSet.next()) {
//                String username = new String(m_ResultSet.getBytes(1), "UTF-8");
//                int edits_in_timeframe = m_ResultSet.getInt(2);
//                String isnoob = edits_in_timeframe > 0 ? "no" : "yes";
//                String gender = new String(m_ResultSet.getBytes(3), "UTF-8");
//                String user_id = new String(m_ResultSet.getBytes(4), "UTF-8");
//                String gu_id = new String(m_ResultSet.getBytes(5), "UTF-8");
//                //cs_articles.add(new CEESParsedUkwikiListedArticle("Talk:" + article_page_title, user_name));
//                CEESUser ceesUser = new CEESUser(user_id, gu_id, username, gender, isnoob);
//                usersdata.put(username, ceesUser);
//                System.out.println("Fetcher:\t" + ceesUser);
//                // System.out.println("Course\t#" + course_id + "\t" + course_title + ",\tarticle\t" + article_page_title + "\tby\t" + user_name);
//            }
            con.close();
            doSshTunnel.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(668);
        }

        return null;
    }//function end
}//class end
