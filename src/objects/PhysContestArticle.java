package objects;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class PhysContestArticle extends Article{

    public int size;
    public int initialsize;
    public String category;

    public PhysContestArticle(
            String articlename,
            String curid,
            String user,
            String userid,
            int size,
            int initialsize,
            String category) {
        this.articlename = articlename;
        this.curid = curid;
        this.user = user;
        this.userid = userid;
        this.size = size;
        this.initialsize = initialsize;
        this.category = category;
    }

}
