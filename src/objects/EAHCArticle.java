package objects;

import java.util.Calendar;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class EAHCArticle extends Article{

    public Calendar timestamp;

    public EAHCArticle(
            String articlename,
            String curid,
            String user,
            String userid,
            String userca,
            Calendar timestamp,
            String language,
            String country,
            String wikidata) {
        this.articlename = articlename;
        this.curid = curid;
        this.user = user;
        this.userid = userid;
        this.userca = userca;
        this.timestamp = timestamp;
        this.language = language;
        this.country = country;
        this.wikidata = wikidata;
    }

}
